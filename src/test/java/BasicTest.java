import dto.response.books.BookResponseDTO;
import org.testng.annotations.Test;
import steps.BooksSteps;
import steps.UserSteps;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicTest {

    @Test
    void start() {
        var tokenDto = new UserSteps().generateToken();

        assertThat(tokenDto.getResult()).isEqualTo("User authorized successfully.");
        //System.out.println(tokenDto);
    }

    @Test
    void getAllBooks() {
        String bookIsbn = new BooksSteps().getALLBooks()
                .getBooks().stream()
                .filter(a -> a.getTitle().length() >= 40)
                .peek(System.out::println)
                .map(BookResponseDTO::getIsbn)
                .findFirst().orElseThrow();

        var book = new BooksSteps().getOneBook(bookIsbn).getIsbn();
        assertThat(book).as("Book is not correct").isEqualTo("9781449337711");


    }


}
