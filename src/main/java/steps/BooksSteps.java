package steps;

import config.configuration.BaseConfig;
import config.configuration.BooksConfig;
import dto.response.books.BookResponseDTO;
import dto.response.books.BooksResponseDTO;

import static config.specification.ResponseSpec.ok;
import static org.aeonbits.owner.ConfigFactory.create;
import static org.aeonbits.owner.ConfigFactory.getProperties;
import static config.base.Requests.get;

public class BooksSteps {

    private final BooksConfig config = create(BooksConfig.class, getProperties());

    public BooksResponseDTO getALLBooks() {
        return get(config.getAllBooks()).spec(ok()).extract().as(BooksResponseDTO.class);
    }

    public BookResponseDTO getOneBook(String bookIsbn) {
        return get(String.format(config.getOneBook(),bookIsbn)).spec(ok()).extract().as(BookResponseDTO.class);
    }
}
